//
//  SelectMusicsViewController.swift
//  MusicApp
//
//  Created by Pedro Albuquerque on 26/12/16.
//  Copyright © 2016 Pedro Albuquerque. All rights reserved.
//

import UIKit
import PulsingHalo

class LoadingViewController: UIViewController {
    
    var screen = (UIApplication.shared.delegate?.window)!
    var viewLoad: UIView!
    var viewBack: UIView!
    //@IBOutlet weak var viewLoading: UIView!
    override func viewDidLoad() {
    }
    
    func createView() {
        
        screen = (UIApplication.shared.delegate?.window)!
        
        let KeyboardExerciseNib = UINib(nibName: "Loading", bundle: nil)
        viewLoad = KeyboardExerciseNib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        self.viewLoad.frame = CGRect(x: (self.screen?.frame.width)!/2 - 100, y: (self.screen?.frame.height)!, width: 200, height: 200)
        viewLoad.backgroundColor = UIColor.clear
        viewLoad.layer.cornerRadius = 20
        viewLoad.clipsToBounds = true
        viewLoad.layer.zPosition = 21
        
        viewBack = UIView(frame: CGRect(x: 0, y: 0, width: (self.screen?.frame.width)!, height: (self.screen?.frame.height)!))
        
        viewBack.backgroundColor = UIColor.black
        viewBack.alpha = 0.3
        viewBack.layer.zPosition = 20
        
        let halo = PulsingHaloLayer()
        halo.radius = 100.0;
        halo.haloLayerNumber = 3;
        halo.backgroundColor = UIColor.white.cgColor
        halo.pulseInterval = 0.1
        halo.position = CGPoint(x: viewLoad.frame.width/2, y: viewLoad.frame.height/2)
        viewLoad.layer.addSublayer(halo)
        halo.start()
        
        self.viewBack.isHidden = true
        screen?.addSubview(viewBack)
        screen?.addSubview(viewLoad)
        
    }
    
    func show() {
        self.viewBack.isHidden = false
        UIView.animate(withDuration: 0.3, animations: { [unowned self] in
            self.viewLoad.frame = CGRect(x: (self.screen?.frame.width)!/2 - 100, y: (self.screen?.frame.height)!/2 - 100, width: 200, height: 200)
        })
    }
    
    func hide() {
        self.viewBack.isHidden = true
        UIView.animate(withDuration: 0.3, animations: { [unowned self] in
            self.viewLoad.frame = CGRect(x: (self.screen?.frame.width)!/2 - 100, y: (self.screen?.frame.height)!, width: 200, height: 200)
        })
    }
}
