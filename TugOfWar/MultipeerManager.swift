//
//  MultipeerManager
//  TugOfWar
//
//  Created by Pedro Albuquerque on 14/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserve.
//

import UIKit
import MultipeerConnectivity

protocol MultipeerDelegate {
    func received(acceptTeam: Bool)
    func session(state: MCSessionState, peerID: MCPeerID)
}

class RemotePlayer {
    let id: String
    let name: String
    let peerID: MCPeerID
    
    init(id: String, name: String, peerID: MCPeerID) {
        self.id = id
        self.name = name
        self.peerID = peerID
    }
}

class MultipeerManager: NSObject {
    
    var delegate: MultipeerDelegate!
    
    var arrConnectedDevices = [RemotePlayer]()
    var arrFoundDevices = [RemotePlayer]()
    
    var peer = MCPeerID.init(displayName: UIDevice.current.name)
    
    let serviceTypePadrao = "tug"
    
    var advertiser: MCNearbyServiceAdvertiser!
    
    override init() {
        
        advertiser = MCNearbyServiceAdvertiser.init(peer: peer, discoveryInfo: ["key":(UIDevice.current.identifierForVendor?.uuidString)!], serviceType: serviceTypePadrao)
        
        super.init()
        
        advertiser.delegate = self
        advertiser.startAdvertisingPeer()
        
    }
    
    lazy var session: MCSession = {
        let sessao = MCSession.init(peer: self.peer)
        sessao.delegate = self
        return sessao
    }()
    
    func startBrowsing(){
        advertiser.startAdvertisingPeer()
    }
    
//    func advertiseSelf(shouldAdvertise: Bool) {
//        startBrowsing()
//        if shouldAdvertise {
//            let dic = ["key": UIDevice.current.identifierForVendor!.uuidString]
//            self.advertiser = MCAdvertiserAssistant(serviceType: serviceTypePadrao, discoveryInfo: dic, session: session)
//            advertiser.start()
//        }else{
//            if let _ = advertiser {
//                advertiser.stop()
//                advertiser = nil
//            }
//        }
//    }

    func find(peerID: MCPeerID, remotes: [RemotePlayer]) -> Int? {
        for (index, remote) in remotes.enumerated() {
            if remote.peerID == peerID {
                return index
            }
        }
        return nil
    }
    
    func sendMessage(direction: Directions){
        let dataToSend: Data? = Data(from: direction.rawValue)
        let allPeers: [Any] = appDelegate.mcManager.session.connectedPeers
        try! appDelegate.mcManager.session.send(dataToSend!, toPeers: allPeers as! [MCPeerID], with: .reliable)
    }
    
    func sendMessage(team: Int){
        let dataToSend: Data? = Data(from: team+10)
        let allPeers: [Any] = appDelegate.mcManager.session.connectedPeers
        try! appDelegate.mcManager.session.send(dataToSend!, toPeers: allPeers as! [MCPeerID], with: .reliable)
    }
}

extension MultipeerManager: MCNearbyServiceAdvertiserDelegate {
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        print("recebi convite")
        
        invitationHandler(true, session)
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) { }
}

extension MultipeerManager: MCNearbyServiceBrowserDelegate {
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        
        if let key = info?["key"] {
            let newRemotePlayer = RemotePlayer(id: key, name: peerID.displayName, peerID: peerID)
            if let remoteIndex = find(peerID: newRemotePlayer.peerID, remotes: arrFoundDevices){
                arrFoundDevices[remoteIndex] = newRemotePlayer
            }else{
                arrFoundDevices.append(newRemotePlayer)
            }
        }
        
        //self.browser.invitePeer(peerID, to: session, withContext: nil, timeout: 30)
        
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) { }
    

}


extension MultipeerManager: MCSessionDelegate {
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        if data.to(type: Int.self) == 21 {
            delegate.received(acceptTeam: true)
        }else if data.to(type: Int.self) == 20 {
            delegate.received(acceptTeam: false)
        }
    }
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        if find(peerID: peerID, remotes: arrFoundDevices) == nil {
            delegate.session(state: state, peerID: peerID)
        }
        switch state {
        case .connected:
            // PEER CONNECTED
            if let remoteIndex = find(peerID: peerID, remotes: arrFoundDevices) {
                self.arrConnectedDevices.append(arrFoundDevices[remoteIndex])
            }
        case .connecting:
            // PEER IS CONNECTING
            break
        case .notConnected:
            
            // PEER DISCONNECTED
            
            if self.arrConnectedDevices.count > 0 {
                if let remoteIndex = find(peerID: peerID, remotes: arrConnectedDevices) {
                    
                    self.arrConnectedDevices.remove(at: remoteIndex)
                }
            }
            
            if let remoteIndex = find(peerID: peerID, remotes: arrFoundDevices) {
                arrFoundDevices.remove(at: remoteIndex)
            }
        }
        print(self.arrConnectedDevices)
        
    }
    
    func session(_ session: MCSession, didReceiveCertificate certificate: [Any]?, fromPeer peerID: MCPeerID, certificateHandler: @escaping (Bool) -> Void) {
        certificateHandler(true)
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) { }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) { }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL, withError error: Error?) { }
    
}

//import UIKit
//import MultipeerConnectivity
//
//
//protocol MultiPeerDelegate {
//    func conectado(nome: String)
//}
//
//class MultiPeer: NSObject {
//    
//    var peer = MCPeerID.init(displayName: UIDevice.current.name)
//    let serviceTypePadrao = "pong"
//    var advertiser: MCNearbyServiceAdvertiser
//    var displayNameMaster: String?
//    
//    var delegate: MultiPeerDelegate?
//    
//    override init() {
//        
//        advertiser = MCNearbyServiceAdvertiser.init(peer: peer, discoveryInfo: ["key":(UIDevice.current.identifierForVendor?.uuidString)!], serviceType: serviceTypePadrao)
//        
//        super.init()
//        
//        advertiser.delegate = self
//        advertiser.startAdvertisingPeer()
//        
//    }
//    
//    lazy var session: MCSession = {
//        let sessao = MCSession.init(peer: self.peer)
//        sessao.delegate = self
//        return sessao
//    }()
//    
//}
//
//extension MultiPeer: MCNearbyServiceAdvertiserDelegate {
//    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
//        print("recebi convite")
//        
//        invitationHandler(true, session)
//    }
//    
//    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) { }
//}
//
//
//extension MultiPeer: MCSessionDelegate {
//    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
//        
//    }
//    
//    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
//        
//        switch state {
//        case .connected:
//            guard let _ = self.displayNameMaster else {
//                self.displayNameMaster = peerID.displayName
//                print("Conectou \(self.displayNameMaster)")
//                self.delegate?.conectado(nome: self.displayNameMaster!)
//                return
//            }
//            
//            
//        case .connecting:
//            print("Conectando \(peerID.displayName)")
//        case .notConnected:
//            print("Desconectou \(peerID.displayName)")
//        }
//        
//    }
//    
//    func session(_ session: MCSession, didReceiveCertificate certificate: [Any]?, fromPeer peerID: MCPeerID, certificateHandler: @escaping (Bool) -> Void) {
//        certificateHandler(true)
//    }
//    
//    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) { }
//    
//    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
//        
//    }
//    
//    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL, withError error: Error?) {
//        
//    }
//    
//}
