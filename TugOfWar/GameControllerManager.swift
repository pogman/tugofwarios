//
//  GameController.swift
//  TugOfWarTV
//
//  Created by Pedro Albuquerque on 10/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import MultipeerConnectivity

protocol GameControllerDelegate: class {
    func received(acceptTeam: Bool)
}

class GameControllerManager {
    
    weak var delegate: GameControllerDelegate!
    var arrConnectedDevices = [String]()
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveData), name: NSNotification.Name(rawValue: "MCDidReceiveDataNotification"), object: nil)
    }
    
    func sendMessage(direction: Directions){
        let dataToSend: Data? = Data(from: direction.rawValue)
        let allPeers: [Any] = appDelegate.mcManager.session.connectedPeers
        try! appDelegate.mcManager.session.send(dataToSend!, toPeers: allPeers as! [MCPeerID], with: .reliable)
    }
    
    func sendMessage(team: Int){
        let dataToSend: Data? = Data(from: team+10)
        let allPeers: [Any] = appDelegate.mcManager.session.connectedPeers
        try! appDelegate.mcManager.session.send(dataToSend!, toPeers: allPeers as! [MCPeerID], with: .reliable)
    }
    
    @objc func didReceiveData(notification: NSNotification){
        //let peerID: MCPeerID? = (notification.userInfo?["peerID"] as! MCPeerID)
        //let peerDisplayName: String? = peerID?.displayName
        
        let receivedData: Data? = (notification.userInfo?["data"] as! Data)
        if let dataValue = receivedData?.to(type: Bool.self) {
            delegate.received(acceptTeam: dataValue)
        }
//        [_tvChat performSelectorOnMainThread:@selector(setText:) withObject:[_tvChat.text stringByAppendingString:[NSString stringWithFormat:@"%@ wrote:\n%@\n\n", peerDisplayName, receivedText]] waitUntilDone:NO];
    }
}

extension Data {
    
    init<T>(from value: T) {
        var value = value
        self.init(buffer: UnsafeBufferPointer(start: &value, count: 1))
    }
    
    func to<T>(type: T.Type) -> T {
        return self.withUnsafeBytes { $0.pointee }
    }
}
