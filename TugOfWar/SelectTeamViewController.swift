//
//  SelectTeamViewController.swift
//  TugOfWar
//
//  Created by Pedro Albuquerque on 14/02/17.
//  Copyright © 2017 Pedro Albuquerque. All rights reserved.
//

import Foundation
import MultipeerConnectivity

enum Directions: Int {
    case bottom = 0, right, top, left
}

class SelectTeamViewController: UIViewController {
    
    @IBOutlet weak var imgRed: UIImageView!
    @IBOutlet weak var imgBlue: UIImageView!
    @IBOutlet weak var imgMini: UIImageView!
    @IBOutlet weak var lbMini: UILabel!
    
    @IBOutlet weak var touchConsole: UIView!
    @IBOutlet weak var backConsole: UIView!
    @IBOutlet weak var viewTeamUp: FocusControl!
    @IBOutlet weak var viewTeamDown: FocusControl!
    @IBOutlet weak var viewRemote: UIView!
    
    @IBOutlet weak var lbPress: UILabel!
    var teams = [[RemotePlayer](), [RemotePlayer]()]
    
    var animatorConsole:UIDynamicAnimator!
    var snap: UISnapBehavior!
    
    var teamSelected: Int?
    
    let loading = LoadingViewController()
    
    var dragStartPositionRelativeToCenter : CGPoint?
    
    override func viewDidLayoutSubviews() {
        touchConsole.layer.cornerRadius = touchConsole.frame.size.width/2
        touchConsole.clipsToBounds = true
        
        touchConsole.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(handlePan)))
        
        touchConsole.layer.shadowColor = UIColor.black.cgColor
        touchConsole.layer.shadowOffset = CGSize(width: 0, height: 3)
        touchConsole.layer.shadowOpacity = 0.5
        touchConsole.layer.shadowRadius = 2
        
        
        backConsole.layer.zPosition = 9
        touchConsole.layer.zPosition = 10
        
        animatorConsole = UIDynamicAnimator(referenceView: self.view)
        snap = UISnapBehavior(item: self.touchConsole, snapTo: self.view.center)
        snap.damping = 2
        
        loading.createView()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchConnection()
        
        appDelegate.mcManager.delegate = self
        appDelegate.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //searchConnection()
    }
    
    @IBAction func hangleSwipeRight(_ sender: Any) {
        appDelegate.mcManager.sendMessage(direction: .right)
    }
    
    @IBAction func hangleSwipeLeft(_ sender: Any) {
        appDelegate.mcManager.sendMessage(direction: .left)
    }
    
    @IBAction func hangleSwipeUp(_ sender: Any) {
        appDelegate.mcManager.sendMessage(direction: .top)
    }
    
    @IBAction func hangleSwipeDown(_ sender: Any) {
        appDelegate.mcManager.sendMessage(direction: .bottom)
    }
    
    func peerDidChangeStateWithNotification(notification: NSNotification) {
        var peerID: MCPeerID? = (notification.userInfo?["peerID"] as? MCPeerID)
        let state: MCSessionState? = MCSessionState.init(rawValue: notification.userInfo?["state"] as! Int)
        
        if state != .connecting {
            print("Connecting")
            if state == .connected {
                DispatchQueue.main.async {
                    self.loading.hide()
                    self.lbPress.isHidden = false
                    //self.reloadConnection.moveTop()
                }
            }
            else if state == .notConnected {
                self.searchConnection()
            }
        }
    }
    
    fileprivate func searchConnection(){
        DispatchQueue.main.async {
            //self.reloadConnection.show()
            self.loading.show()
            self.lbPress.isHidden = true
            
            self.reloadViews()
        }
        
       // appDelegate.mcManager.advertiseSelf(shouldAdvertise: true)
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(peerDidChangeStateWithNotification), name: NSNotification.Name(rawValue: "MCDidChangeStateNotification"), object: nil)
    }
    
    fileprivate func reloadViews(){
        self.teamSelected = nil
        UIView.animate(withDuration: 0.1, animations: { [unowned self] in
            self.viewRemote.isHidden = true
            self.viewRemote.layer.zPosition = 100
            self.viewTeamUp.transform = CGAffineTransform.identity
            self.viewTeamDown.transform = CGAffineTransform.identity
            self.touchConsole.alpha = 1
            self.viewTeamUp.layer.shadowOpacity = 0
            self.viewTeamDown.layer.shadowOpacity = 0
            self.imgRed.alpha = 1
            self.imgBlue.alpha = 1
            self.imgMini.alpha = 0
            self.lbMini.alpha = 0
            self.backConsole.frame = CGRect(x: (self.view?.frame.width)!/2, y: (self.view?.frame.height)!/2, width: 0, height: 0)
            self.backConsole.isHidden = false
            
            
        })
        
        self.dragStartPositionRelativeToCenter = nil
        
        self.animatorConsole.removeAllBehaviors()
        self.animatorConsole.addBehavior(self.snap)
    }
    
    //GESTURES
    
    func handlePan(nizer: UILongPressGestureRecognizer!) {
        
        let centerBoardY = self.view.center.y
        let centerBoardX = self.view.center.x
        let centerRondY = nizer.view?.center.y
        let centerRondX = nizer.view?.center.x
        
        if centerRondY! + 0 < centerBoardY - 5 {
            self.teamSelected = 0
            UIView.animate(withDuration: 0.1, animations: { [unowned self] in
                self.viewTeamUp.transform = CGAffineTransform.identity
                self.viewTeamUp.layer.shadowOpacity = 0
                self.viewTeamDown.layer.zPosition = 1
                self.viewTeamUp.layer.zPosition = 0
                
                self.viewTeamDown.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
                self.viewTeamDown.layer.shadowOpacity = 0.2
                self.viewTeamDown.layer.shadowOffset = CGSize(width: -10, height: -10)
            })
            
        }else if centerRondY! + 0 > centerBoardY + 5 {
            self.teamSelected = 1
            UIView.animate(withDuration: 0.1, animations: { [unowned self] in
                self.viewTeamDown.transform = CGAffineTransform.identity
                self.viewTeamDown.layer.shadowOpacity = 0
                self.viewTeamUp.layer.zPosition = 1
                self.viewTeamDown.layer.zPosition = 0
                
                self.viewTeamUp.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
                self.viewTeamUp.layer.shadowOpacity = 0.2
                self.viewTeamUp.layer.shadowOffset = CGSize(width: 10, height: 10)
            })
        }
        
//        if centerRondY! + 0 > centerBoardY + 50 || centerRondY! + 0 < centerBoardY - 50 || centerRondX! + 0 > centerBoardX + 50 || centerRondX! + 0 < centerBoardX - 50 {
//            if teamSelected != nil {
//                appDelegate.mcManager.sendMessage(team: teamSelected!)
//            }
//            
//            return
//        }
        
        if nizer.state == UIGestureRecognizerState.began {
            UIView.animate(withDuration: 0.3, animations: { [unowned self] in
                self.backConsole.frame = CGRect(x: (self.view?.frame.width)!/2 - 60, y: (self.view?.frame.height)!/2 - 60, width: 120, height: 120)
                self.backConsole.layer.cornerRadius = self.backConsole.frame.width/2
            })
            
            let locationInView = nizer.location(in: self.view)
            dragStartPositionRelativeToCenter = CGPoint(x: locationInView.x - touchConsole.center.x, y: locationInView.y - touchConsole.center.y)
            
            touchConsole.layer.shadowOffset = CGSize(width: 0, height: 20)
            touchConsole.layer.shadowOpacity = 0.3
            touchConsole.layer.shadowRadius = 6
            
            return
        }
        
        if nizer.state == UIGestureRecognizerState.ended && dragStartPositionRelativeToCenter != nil {
            if teamSelected != nil {
                appDelegate.mcManager.sendMessage(team: teamSelected!)
            }
            
            return
        }
        
        let locationInView = nizer.location(in: self.view)
        if let drag = dragStartPositionRelativeToCenter{
            UIView.animate(withDuration: 0.1) {
                self.touchConsole.center = CGPoint(x: locationInView.x - drag.x,
                                                   y: locationInView.y - drag.y)
            }
        }
    }
    
    fileprivate func touchConsoleToEndedTouch(){
        DispatchQueue.main.async {
            self.dragStartPositionRelativeToCenter = nil
            
            UIView.animate(withDuration: 0.5, animations: { [unowned self] in
                self.backConsole.frame = CGRect(x: (self.view?.frame.width)!/2, y: (self.view?.frame.height)!/2, width: 0, height: 0)
                self.backConsole.isHidden = true
                
                self.touchConsole.alpha = 0
                self.lbMini.alpha = 1
                self.lbMini.text = UIDevice.current.name.gadgetName
                
                if let team = self.teamSelected {
                    self.imgRed.alpha = 0
                    self.imgBlue.alpha = 0
                    self.imgMini.alpha = 1
                    if team == 1 {
                        self.imgMini.image = #imageLiteral(resourceName: "blue")
                        self.viewTeamUp.transform = CGAffineTransform(scaleX: 4, y: 4)
                        self.viewTeamUp.layer.zPosition = 1
                        self.viewTeamDown.layer.zPosition = 0
                    }else{
                        self.imgMini.image = #imageLiteral(resourceName: "red2")
                        self.viewTeamDown.transform = CGAffineTransform(scaleX: 4, y: 4)
                        self.viewTeamUp.layer.zPosition = 0
                        self.viewTeamDown.layer.zPosition = 1
                    }
                }
            })
            
            self.viewRemote.isHidden = false
            
            self.touchConsole.layer.shadowOffset = CGSize(width: 0, height: 3)
            self.touchConsole.layer.shadowOpacity = 0.5
            self.touchConsole.layer.shadowRadius = 2
            
            self.animatorConsole.removeAllBehaviors()
            self.animatorConsole.addBehavior(self.snap)
        }
    }
    
    fileprivate func find(remote: RemotePlayer, team: Int) -> Int? {
        for (index, r) in teams[team].enumerated() {
            if r.id == r.id {
                return index
            }
        }
        return nil
    }
}

extension SelectTeamViewController: MultipeerDelegate {
    func received(acceptTeam: Bool) {
        if acceptTeam {
            print("aceito")
            self.touchConsoleToEndedTouch()
        }else{
            print("nao aceito")
            DispatchQueue.main.async {
                self.reloadViews()
                
                let alert = UIAlertController(title: "Team Full", message: "Team with many players.", preferredStyle: UIAlertControllerStyle.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func session(state: MCSessionState, peerID: MCPeerID) {
        if state == .connected {
            DispatchQueue.main.async {
                self.loading.hide()
                self.lbPress.isHidden = false
                //self.reloadConnection.moveTop()
                //appDelegate.mcManager.advertiseSelf(shouldAdvertise: false)
            }
        }
        else if state == .notConnected {
            appDelegate.mcManager.session.disconnect()
            appDelegate.mcManager = MultipeerManager()
            searchConnection()
            appDelegate.mcManager.delegate = self
        }
    }
}

extension SelectTeamViewController: ReloadConnectionDelegate {
    func reloadConnectionClicked() {
        appDelegate.mcManager.session.disconnect()
        appDelegate.mcManager = MultipeerManager()
        searchConnection()
        appDelegate.mcManager.delegate = self
    }
}

extension SelectTeamViewController: ApplicationDelegate {
    func applicationEnterForeground() {
        appDelegate.mcManager.session.disconnect()
        appDelegate.mcManager = MultipeerManager()
        searchConnection()
        appDelegate.mcManager.delegate = self
    }
}

extension String {
    func slice(from: String, to: String) -> String? {
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                substring(with: substringFrom..<substringTo)
            }
        }
    }
    
    var gadgetName: String {
        if (self.slice(from: "i", to: "de")) != nil {
            if "\(self.slice(from: "i", to: "de")!)" == "Phone " {
                var componentes = self.components(separatedBy: " de ")
                return componentes[1]
            }
        } else if (self.slice(from: "’", to: "ne")) != nil {
            if "\(self.slice(from: "’", to: "ne")!)" == "s iPho" {
                var componentes = self.components(separatedBy: "’s ")
                return componentes[0]
            }
        }
        return self
    }
}

