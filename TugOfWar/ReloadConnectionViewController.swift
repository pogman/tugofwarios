//
//  SelectMusicsViewController.swift
//  MusicApp
//
//  Created by Pedro Albuquerque on 26/12/16.
//  Copyright © 2016 Pedro Albuquerque. All rights reserved.
//

import UIKit

protocol ReloadConnectionDelegate: class {
    func reloadConnectionClicked()
}

class ReloadConnectionViewController: UIViewController {
    
    var screen = (UIApplication.shared.delegate?.window)!
    var viewLoad: UIView!
    @IBOutlet weak var btnReload: UIButton!
    @IBOutlet weak var btnDisconnect: UIButton!
    
    weak var delegate: ReloadConnectionDelegate!
    
    override func viewDidLoad() {
    }
    
    func createView() {
        
        screen = (UIApplication.shared.delegate?.window)!
        
        let KeyboardExerciseNib = UINib(nibName: "ReloadConnection", bundle: nil)
        viewLoad = KeyboardExerciseNib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        self.viewLoad.frame = CGRect(x: 10, y: (self.screen?.frame.height)!, width: (self.screen?.frame.width)! - 20, height: 48)
        
        viewLoad.backgroundColor = UIColor.white
        viewLoad.layer.cornerRadius = 20
        viewLoad.clipsToBounds = true
        viewLoad.layer.zPosition = 30
    
        screen?.addSubview(viewLoad)
        
    }
    
    @IBAction func btnReconnectAction(_ sender: Any) {
        delegate.reloadConnectionClicked()
    }
    
    func show() {
        self.btnReload.isHidden = false
        self.btnDisconnect.isHidden = true
        UIView.animate(withDuration: 0.3, animations: { [unowned self] in
            self.viewLoad.frame = CGRect(x: 10, y: (self.screen?.frame.height)! - 58, width: (self.screen?.frame.width)! - 20, height: 48)
        })
    }
    
    func moveTop() {
        UIView.animate(withDuration: 0.3, animations: { [unowned self] in
            self.btnReload.isHidden = true
            self.btnDisconnect.isHidden = false
            self.viewLoad.frame = CGRect(x: 10, y: 25, width: (self.screen?.frame.width)! - 20, height: 48)
        })
    }
    
    func moveDown() {
        self.btnReload.isHidden = false
        self.btnDisconnect.isHidden = true
        UIView.animate(withDuration: 0.3, animations: { [unowned self] in
            self.viewLoad.frame = CGRect(x: 10, y: (self.screen?.frame.height)! - 58, width: (self.screen?.frame.width)! - 20, height: 48)
        })
    }
    
    func hide() {
        self.btnReload.isHidden = false
        self.btnDisconnect.isHidden = true
        UIView.animate(withDuration: 0.3, animations: { [unowned self] in
            self.viewLoad.frame = CGRect(x: 10, y: (self.screen?.frame.height)!, width: (self.screen?.frame.width)! - 20, height: 48)
        })
    }
}
